package ru.edu.simple.list;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class LinkedSimpleListTest {

    private static final String BASE_ELEMENT_STRING = "Строка";
    private static final String NEW_ELEMENT_STRING = "Новая строка";
    private static final int MAX_ARRAY_LENGTH = 12;
    private static final int ELEMENT_NUMBER_TO_VERIFY_SET = 5;

    LinkedSimpleList<String> linkedSimpleList;

    private void fillList(LinkedSimpleList<String> list) {
        for (int i = 0; i < MAX_ARRAY_LENGTH; i++) {
            list.add(BASE_ELEMENT_STRING + i);
        }
    }

    private void setListElements(LinkedSimpleList<String> list) {
        list.set(MAX_ARRAY_LENGTH, NEW_ELEMENT_STRING);
        list.set(ELEMENT_NUMBER_TO_VERIFY_SET, NEW_ELEMENT_STRING);
    }

    private LinkedSimpleList<String> getExpectedListForRemoveTest() {
        LinkedSimpleList<String> list = new LinkedSimpleList<>();
        for (int i = 0; i < MAX_ARRAY_LENGTH - 2; i++) {
            int index = i < 5 ? i : i + 1;
            list.add(BASE_ELEMENT_STRING + index);
        }
        return list;
    }

    @Before
    public void setUp() {
        linkedSimpleList = new LinkedSimpleList<>();
        fillList(linkedSimpleList);
    }

    @Test
    public void listNotNull() {
        assertNotNull(linkedSimpleList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBound1() {
        linkedSimpleList.set(MAX_ARRAY_LENGTH + 1, NEW_ELEMENT_STRING);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBound2() {
        linkedSimpleList.set(-1, NEW_ELEMENT_STRING);
    }

    @Test
    public void set() {
        setListElements(linkedSimpleList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBound1() {
        linkedSimpleList.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBound2() {
        linkedSimpleList.get(MAX_ARRAY_LENGTH + 2);
    }

    @Test
    public void get() {
        setListElements(linkedSimpleList);
        for (int i = 0; i < linkedSimpleList.size(); i++) {
            String expectedValue;
            if (i == ELEMENT_NUMBER_TO_VERIFY_SET || i == MAX_ARRAY_LENGTH)
                expectedValue = NEW_ELEMENT_STRING;
            else
                expectedValue = BASE_ELEMENT_STRING + i;
            assertEquals(linkedSimpleList.get(i), expectedValue);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBound1() {
        linkedSimpleList.remove(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBound2() {
        linkedSimpleList.remove(MAX_ARRAY_LENGTH);
    }

    @Test
    public void remove() {
        linkedSimpleList.remove(MAX_ARRAY_LENGTH - 1);
        linkedSimpleList.remove(5);
        assertEquals(linkedSimpleList, getExpectedListForRemoveTest());
        linkedSimpleList.remove(0);
        assertEquals(BASE_ELEMENT_STRING + "1", linkedSimpleList.get(0));
    }

    @Test
    public void indexOf() {
        assertEquals(5, linkedSimpleList.indexOf(BASE_ELEMENT_STRING + "5"));
        assertEquals(-1, linkedSimpleList.indexOf(BASE_ELEMENT_STRING + "1000"));
        int endIndex = linkedSimpleList.size() - 1;
        for (int i = 0; i < endIndex; i++) {
            linkedSimpleList.remove(linkedSimpleList.size() - 1);
        }
        assertEquals(0, linkedSimpleList.indexOf(BASE_ELEMENT_STRING + "0"));
        linkedSimpleList.remove(0);
        assertEquals(-1, linkedSimpleList.indexOf(BASE_ELEMENT_STRING));
    }

    @Test
    public void size() {
        assertEquals(MAX_ARRAY_LENGTH, linkedSimpleList.size());
    }

    @Test
    public void hashCodeTest() {
        LinkedSimpleList<String> anotherList = new LinkedSimpleList<>();
        fillList(anotherList);
        assertEquals(
                anotherList.hashCode(),
                linkedSimpleList.hashCode());
    }

    @Test
    public void equalsTest() {
        assertEquals(linkedSimpleList, linkedSimpleList);
        assertNotEquals(new LinkedSimpleList<>(), linkedSimpleList);
        LinkedSimpleList<String> anotherList = new LinkedSimpleList<>();
        fillList(anotherList);
        assertEquals(anotherList, linkedSimpleList);
        assertNotEquals(null, linkedSimpleList);
        assertNotEquals(BASE_ELEMENT_STRING, linkedSimpleList);
        assertNotEquals(linkedSimpleList, BASE_ELEMENT_STRING);
        anotherList.set(anotherList.size() - 1, "Другая строка");
        assertNotEquals(anotherList, linkedSimpleList);
    }
}