package ru.edu.simple.list;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ArraySimpleListTest {

    private static final String BASE_ELEMENT_STRING = "Строка";
    private static final String NEW_ELEMENT_STRING = "Новая строка";
    private static final int MAX_ARRAY_LENGTH = 12;
    private static final int ELEMENT_NUMBER_TO_VERIFY_SET = 5;

    ArraySimpleList<String> arraySimpleList;

    private void fillList(ArraySimpleList<String> list) {
        for (int i = 0; i < MAX_ARRAY_LENGTH; i++) {
            list.add(BASE_ELEMENT_STRING + i);
        }
    }

    private void setListElements(ArraySimpleList<String> list) {
        list.set(MAX_ARRAY_LENGTH, NEW_ELEMENT_STRING);
        list.set(ELEMENT_NUMBER_TO_VERIFY_SET, NEW_ELEMENT_STRING);
    }

    private ArraySimpleList<String> getExpectedListForRemoveTest() {
        ArraySimpleList<String> list = new ArraySimpleList<>();
        for (int i = 0; i < MAX_ARRAY_LENGTH - 2; i++) {
            int index = i < 5 ? i : i + 1;
            list.add(BASE_ELEMENT_STRING + index);
        }
        return list;
    }

    @Before
    public void setUp() {
        arraySimpleList = new ArraySimpleList<>();
        fillList(arraySimpleList);
    }

    @Test
    public void listNotNull() {
        assertNotNull(arraySimpleList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBound1() {
        arraySimpleList.set(MAX_ARRAY_LENGTH + 1, NEW_ELEMENT_STRING);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBound2() {
        arraySimpleList.set(-1, NEW_ELEMENT_STRING);
    }

    @Test
    public void set() {
        setListElements(arraySimpleList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBound1() {
        arraySimpleList.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBound2() {
        arraySimpleList.get(MAX_ARRAY_LENGTH + 2);
    }

    @Test
    public void get() {
        setListElements(arraySimpleList);
        for (int i = 0; i < arraySimpleList.size(); i++) {
            String expectedValue;
            if (i == ELEMENT_NUMBER_TO_VERIFY_SET || i == MAX_ARRAY_LENGTH)
                expectedValue = NEW_ELEMENT_STRING;
            else
                expectedValue = BASE_ELEMENT_STRING + i;
            assertEquals(arraySimpleList.get(i), expectedValue);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBound1() {
        arraySimpleList.remove(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBound2() {
        arraySimpleList.remove(MAX_ARRAY_LENGTH);
    }

    @Test
    public void remove() {
        arraySimpleList.remove(MAX_ARRAY_LENGTH - 1);
        arraySimpleList.remove(5);
        assertEquals(arraySimpleList, getExpectedListForRemoveTest());
    }

    @Test
    public void indexOf() {
        assertEquals(5, arraySimpleList.indexOf(BASE_ELEMENT_STRING + "5"));
        assertEquals(-1, arraySimpleList.indexOf(BASE_ELEMENT_STRING + "1000"));
    }

    @Test
    public void size() {
        assertEquals(arraySimpleList.size(), MAX_ARRAY_LENGTH);
    }

    @Test
    public void hashCodeTest() {
        ArraySimpleList<String> anotherList = new ArraySimpleList<>();
        fillList(anotherList);
        assertEquals(
                anotherList.hashCode(),
                arraySimpleList.hashCode());
    }

    @Test
    public void equalsTest() {
        assertEquals(arraySimpleList, arraySimpleList);
        assertNotEquals(new ArraySimpleList<>(), arraySimpleList);
        ArraySimpleList<String> anotherList = new ArraySimpleList<>();
        fillList(anotherList);
        assertEquals(anotherList, arraySimpleList);
        assertNotEquals(arraySimpleList, null);
        assertNotEquals(null, arraySimpleList);
        assertNotEquals(arraySimpleList, BASE_ELEMENT_STRING);
        assertNotEquals(BASE_ELEMENT_STRING, arraySimpleList);
        anotherList.set(anotherList.size() - 1, "Другая строка");
        assertNotEquals(anotherList, arraySimpleList);
        anotherList.remove(anotherList.size() - 1);
    }
}