package ru.edu.simple.list;

import ru.edu.SimpleList;

import java.util.Arrays;
import java.util.Objects;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * Константа для метода hashcode().
     */
    private static final int HASH_CODE_CONSTANT = 31;

    /**
     * Длина массиива для генерации тестовых данных.
     */
    private static final int INIT_ARRAY_LENGTH = 10;

    /**
     * Количество элементов в списке.
     */
    private int numberOfElements;

    /**
     * Массив элементов.
     */
    private T[] elements;

    /**
     * Конструктор.
     */
    @SuppressWarnings("unchecked")
    public ArraySimpleList() {
        elements = (T[]) new Object[INIT_ARRAY_LENGTH];
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (numberOfElements >= elements.length) {
            increaseListSize();
        }
        elements[numberOfElements] = value;
        numberOfElements++;
    }

    /**
     * Увеличивает размер внутреннего массива.
     */
    @SuppressWarnings("unchecked")
    private void increaseListSize() {
        T[] newArray = (T[]) new Object[elements.length * 2 + 1];
        System.arraycopy(elements, 0, newArray, 0, elements.length);
        elements = newArray;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index > numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else if (index == numberOfElements) {
            add(value);
        } else {
            elements[index] = value;
        }
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (index >= numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else {
            return elements[index];
        }
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index >= numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else {
            if (index < numberOfElements - 1) {
                System.arraycopy(
                        elements,
                        index + 1,
                        elements,
                        index,
                        numberOfElements - (index + 1));
            }
            numberOfElements--;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        for (int i = 0; i < numberOfElements; i++) {
            if (Objects.equals(value, elements[i])) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return numberOfElements;
    }

    /**
     * Метод equals.
     *
     * @param o - объект для сравнения
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArraySimpleList<?> that = (ArraySimpleList<?>) o;
        if (size() != that.size()) {
            return false;
        } else {
            for (int i = 0; i < size(); i++) {
                if (!Objects.equals(get(i), that.get(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Получение хэш кода.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(numberOfElements);
        result = HASH_CODE_CONSTANT * result
                + Arrays.hashCode(Arrays.copyOfRange(elements, 0, size()));
        return result;
    }
}
