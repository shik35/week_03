package ru.edu.simple.list;

import ru.edu.SimpleList;

import java.util.Objects;

public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * Константа для метода hashcode().
     */
    private static final int HASH_CODE_CONSTANT = 31;

    private static final class ListNode<T> {

        /**
         * Предыдущий элемент.
         */
        private ListNode<T> prev;

        /**
         * Значение.
         */
        private T value;

        /**
         * Слежующий элемент.
         */
        private ListNode<T> next;

        ListNode(final T valueToSet) {
            this.value = valueToSet;
        }
    }

    /**
     * Голова списка.
     */
    private ListNode<T> head;

    /**
     * Хвост списка.
     */
    private ListNode<T> tail;

    /**
     * Количество элементов в списке.
     */
    private int numberOfElements;

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (head == null) {
            head = new ListNode<>(value);
            tail = head;
        } else {
            ListNode<T> newNode = new ListNode<>(value);
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        }
        numberOfElements++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index > numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else if (index == numberOfElements) {
            add(value);
        } else {
            ListNode<T> nodeToSet = findNodeByIndex(index);
            nodeToSet.value = value;
        }
    }

    /**
     * Поиск узла по индексу.
     *
     * @param index индекс
     * @return ListNode<T>
     */
    ListNode<T> findNodeByIndex(final int index) {
        if (index >= numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else {
            ListNode<T> resultNode = head;
            for (int i = 0; i < index; i++) {
                resultNode = resultNode.next;
            }
            return resultNode;
        }
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        return findNodeByIndex(index).value;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index >= numberOfElements || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Индекс элемента вышел за границы списка");
        } else {
            if (numberOfElements == 1) {
                head = null;
                tail = null;
            } else if (index == 0) {
                head = head.next;
                head.prev = null;
            } else if (index == numberOfElements - 1) {
                tail = tail.prev;
                tail.next = null;
            } else {
                ListNode<T> nodeToRemove = findNodeByIndex(index);
                nodeToRemove.prev.next = nodeToRemove.next;
                nodeToRemove.next.prev = nodeToRemove.prev;
            }
            numberOfElements--;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        if (numberOfElements > 0) {
            ListNode<T> currentNode = head;
            int currentIndex = 0;
            do {
                if (Objects.equals(value, currentNode.value)) {
                    return currentIndex;
                } else {
                    currentNode = currentNode.next;
                    currentIndex++;
                }
            } while (currentIndex < numberOfElements);
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return numberOfElements;
    }

    /**
     * Метод equals.
     *
     * @param o - объект, с которым нужно сравнить
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LinkedSimpleList)) {
            return false;
        }
        LinkedSimpleList<?> that = (LinkedSimpleList<?>) o;
        if (size() != that.size()) {
            return false;
        } else {
            for (int i = 0; i < size(); i++) {
                if (!Objects.equals(get(i), that.get(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Метод hashCode.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(numberOfElements);
        for (int i = 0; i < numberOfElements; i++) {
            result = result * HASH_CODE_CONSTANT + Objects.hash(get(i));
        }
        return result;
    }
}
